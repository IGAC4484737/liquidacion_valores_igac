#list_excel_readed_zhg = ("../" + df_rutas_mun_sub_v3.loc[lgl_temp, "whole_path_file"]).apply(lambda x: clean_column_names(pd.read_excel(x)))
def export_dfs_to_excel(excel_name, list_dfs, list_sheets = None, output = True):
    if list_sheets == None:
        list_sheets = ["T." + str(k) for k,s in enumerate(list_dfs)]
    # Get the current date
    current_date = datetime.now()

    # Format the date as "ddmmyy"
    date = current_date.strftime("%y%m%d")

    # Specify the path for the Excel file
    if output:
        excel_file_path = "../output/" + str(date) + excel_name
    else:
        excel_file_path = str(date) + excel_name

    # Create a Pandas Excel writer using XlsxWriter as the engine
    excel_writer = pd.ExcelWriter(excel_file_path, engine='xlsxwriter')

    # Convert the DataFrame to an XlsxWriter Excel object
    for k, df in enumerate(list_dfs):
        print(k)
        df.to_excel(excel_writer, sheet_name=list_sheets[k], index=False)

        # Get the xlsxwriter workbook and worksheet objects
        workbook = excel_writer.book
        worksheet = excel_writer.sheets[list_sheets[k]]

        # Adjust column widths
        column_widths = [max(df[col].astype(str).apply(len).max(), len(col)) for col in df.columns]
        for i, width in enumerate(column_widths):
            worksheet.set_column(i, i, width)

    excel_writer.close()


def contains_pattern(strings, pattern):
    # Create a regex pattern with the 're.IGNORECASE' flag to match case-insensitively
    regex_pattern = re.compile(pattern, re.IGNORECASE)

    # Check if any string in the list matches the pattern
    for string in strings:
        if regex_pattern.search(string):
            return True  # Return True if a match is found
    return False  # Return False if no match is found

def clean_column_names(df):
    # Convert column names to lowercase
    df.columns = df.columns.str.lower()
    # Remove leading and trailing whitespace from column names
    df.columns = df.columns.str.strip()
    # Replace spaces with underscores in column names
    df.columns = df.columns.str.replace(' ', '_')
    df.columns = df.columns.map(unidecode.unidecode)
    return df

def replacement_col_names(x, dict_of_replacements = {"codigo_\*$" : "codigo" ,
                    "\\n": "_",
        "codigo_zona_.+" : "codigo_zona_geoeconomica",
        "valor_.+": "valor",
        "area_.+": "area",
        ".+_area": "area"}):
    
    df_readed = clean_column_names(pd.read_excel(x))
    list_to_replace = df_readed.columns
    
    

    # Use re.sub with the dictionary of replacements
    dict_restored = {}
    for k, files in enumerate(list_to_replace):
        files_or = files
        for pattern, replacement in replacements.items():
            files = re.sub(pattern, replacement, files)
        dict_restored[files_or] = files        
    df_readed.rename(columns = dict_restored, inplace=True)
    df_readed_v2 = df_readed[["codigo", "codigo_zona_geoeconomica", "valor", "area"]]
     
           
    regex_pattern_m2 = re.compile("METROS|Metros|metros|m2|M2", re.IGNORECASE)
    regex_pattern_hect = re.compile("HECT|hect", re.IGNORECASE)
    
    #  | contains_pattern(list_to_replace, "METROS|Metros|metros|m2|M2")
    #  | contains_pattern(list_to_replace, "HECT|hect")
    lgl_m2 = False
    if regex_pattern_m2.search(x):
        lgl_m2 = True  # Return True if a match is found
        df_readed_v2["UNIDADES"] = "METROS_CUADRADOS"
    if contains_pattern(list_to_replace, "METROS|Metros|metros|m2|M2"):
        lgl_m2 = True  # Return True if a match is found
        df_readed_v2["UNIDADES"] = "HECTAREAS"
    
    lgl_hect = False
    if regex_pattern_hect.search(x):
        lgl_hect = True  # Return True if a match is found 
        df_readed_v2["unidades"] = "METROS_CUADRADOS"
   
    if contains_pattern(list_to_replace, "HECT|hect"):
        lgl_hect = True  # Return True if a match is found
        df_readed_v2["unidades"] = "HECTAREAS"
    
    # lgl_hect = (df_to_check_zhg["File"].str.contains("HECT|hect", regex = True))
    # lgl_m2 = (df_to_check_zhg["File"].str.contains(, regex = True))
    # df_readed_v2["lgl_hect"] = lgl_hect
    # df_readed_v2["lgl_m2"] = lgl_m2
    
    return df_readed_v2
    #return {"df_result" : df_readed_v2, "lgl_m2" : lgl_m2, "lgl_hect" : lgl_hect}
